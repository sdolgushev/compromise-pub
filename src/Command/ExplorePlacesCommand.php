<?php

namespace App\Command;

use App\Utils\GeoPoint;
use DateTime;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Places;
use App\Service\Geo;
use SKAgarwal\GoogleApi\PlacesApi;
use App\Entity\Place;

class ExplorePlacesCommand extends Command
{
    // Tower of London
    const EXPLORE_COVERAGE_CENTER_LAT = 51.508530;
    const EXPLORE_COVERAGE_CENTER_LON = -0.076132;
    // In meters
    const EXPLORE_COVERAGE_RADIUS = 16000;

    const EXPLORE_RADIUS = 300;
    // https://developers.google.com/places/web-service/search#PlaceSearchPaging
    const EXPLORE_LIMIT = 60;
    // https://stackoverflow.com/questions/21265756/paging-on-google-places-api-returns-status-invalid-request
    const EXPLORE_PAGINATION_DELAY = 2;

    protected static $defaultName = 'app:explore-places';

    private $doctrine;
    private $logger;
    private $placesApi;
    private $places;
    private $geo;
    private $placesToExplore = [];
    private $discoveredPlaces = 0;
    private $requestsCount = 0;
    private $io = null;

    public function __construct(
        ManagerRegistry $doctrine,
        LoggerInterface $logger,
        PlacesApi $placesApi,
        Places $places,
        Geo $geo
    ) {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
        $this->placesApi = $placesApi;
        $this->places = $places;
        $this->geo = $geo;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Explores new places around existing ones')
            ->addArgument('place_id', InputArgument::OPTIONAL, 'Place to explore')
            ->addOption('api_requests_limit', null, InputOption::VALUE_OPTIONAL, 'Scripts stops after API requests limit will be reached', 9)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $placeId = $input->getArgument('place_id');
        $requestsLimit = $input->getOption('api_requests_limit');

        $repository = $this->doctrine->getRepository(Place::class);
        $this->placesToExplore = $repository->findToExplore($placeId);

        if (count($this->placesToExplore) === 0) {
            $this->io->success('We do not have any unexplored places');
            return;
        }

        $this->io->writeln('Starting with ' . count($this->placesToExplore) . ' places ...');
        do {
            $this->explorePlace($requestsLimit);
            if ($this->requestsCount >= $requestsLimit) {
                $this->io->writeln('Stopping the script because API requests limit was hit.');
                break;
            }
        } while (count($this->placesToExplore));

        $this->io->success($this->discoveredPlaces . ' new places were discovered');
    }

    protected function explorePlace(int $requestsLimit): void
    {
        $place = array_shift($this->placesToExplore);
        $this->io->section('[Exploring] Around ' . $place . ' (API Requests: ' . $this->requestsCount . '/' . $requestsLimit . ') ...' );

        $items = $this->getNearByPlaces($place);

        $items = $this->filterExistingOnes($items);
        $items = $this->filterOutOfCovarage($items);
        foreach ($items as $item) {
            $discoveredPlace = $this->places->storePlaceGeneralInfo($item);
            $this->placesToExplore[] = $discoveredPlace;
            $this->discoveredPlaces++;
            $this->io->writeLn('[Created] ' . $discoveredPlace . ' ...');
        }

        $place->setExploredAt(new DateTime());
        $em = $this->doctrine->getManager();
        $em->merge($place);
        $em->flush();
    }

    private function getNearByPlaces(Place $place): array
    {
        $results = [];
        $items = [];

        do {
            $pageToken = isset($results['next_page_token']) ? $results['next_page_token'] : null;
            if ($pageToken !== null) {
                sleep(self::EXPLORE_PAGINATION_DELAY);
            }

            try {
                // Can not use requestNearByPlacesUsingFind, because it returns just 1 place
                $results = $this->requestNearByPlacesUsingNearBy($place, $pageToken);
                $items = array_merge($items, $results['results']->all());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                break;
            }
        } while (isset($results['next_page_token']) && count($items) < self::EXPLORE_LIMIT);

        return $items;
    }

    private function requestNearByPlacesUsingFind(Place $place, string $pageToken = null): array
    {
        $this->requestsCount++;

        $location = $place->getLat() . ',' . $place->getLon();
        $params = [
            'locationbias' => 'circle:' . self::EXPLORE_RADIUS . '@' . $location,
            // https://developers.google.com/places/web-service/search#Fields
            'fields' => 'place_id,id,geometry,types,name,icon'
        ];

        $input = 'pub';
        $inputtype = 'textquery';
        $results = $this->placesApi->findPlace($input, $inputtype, $params)->all();
        $results['results'] = $results['candidates'];
        return $results;
    }

    private function requestNearByPlacesUsingNearBy(Place $place, string $pageToken = null): array
    {
        $this->requestsCount++;

        $location = $place->getLat() . ',' . $place->getLon();
        $params = [
            'type' => 'bar',
            'rankby' => 'distance',
            'keyword' => 'pub'
        ];
        if ($pageToken !== null) {
            $params['pagetoken'] = $pageToken;
        }

        $results = $this->placesApi->nearbySearch($location, self::EXPLORE_RADIUS, $params)->all();
        return $results;
    }

    private function filterExistingOnes(array $items): array
    {
        foreach ($items as $k => $item) {
            if ($this->places->findPlace($item['id']) !== null) {
                unset($items[$k]);
            }
        }
        return array_values($items);
    }

    private function filterOutOfCovarage(array $items): array
    {
        foreach ($items as $k => $item) {
            $location = $item['geometry']['location'];
            if ($this->isWithinCoverage($location['lat'], $location['lng']) === false) {
                unset($items[$k]);
            }
        }
        return array_values($items);
    }

    private function isWithinCoverage(float $lat, float $lon): bool
    {
        $location = new GeoPoint($lat, $lon);
        $coverage = new GeoPoint(
            self::EXPLORE_COVERAGE_CENTER_LAT,
            self::EXPLORE_COVERAGE_CENTER_LON
        );
        $distance = $this->geo->getDistance($location, $coverage);
        return $distance <= self::EXPLORE_COVERAGE_RADIUS;
    }
}
