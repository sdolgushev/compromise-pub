<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Places;
use SKAgarwal\GoogleApi\PlacesApi;
use App\Entity\Place;

class UpdatePlacesCommand extends Command
{
    protected static $defaultName = 'app:update-places';

    private $doctrine;
    private $logger;
    private $placesApi;
    private $places;

    public function __construct(
        ManagerRegistry $doctrine,
        LoggerInterface $logger,
        PlacesApi $placesApi,
        Places $places
    ) {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
        $this->placesApi = $placesApi;
        $this->places = $places;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates information about existing places')
            ->addArgument('place_id', InputArgument::OPTIONAL, 'Place to update')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limits the number of places to update', 30)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $placeId = $input->getArgument('place_id');
        $limit = $input->getOption('limit');

        $repository = $this->doctrine->getRepository(Place::class);
        $places = $repository->findToUpdate($placeId, $limit);

        $io->writeln('Updating ' . count($places) . ' places ...');
        $i = 1;
        $c = count($places);
        foreach ($places as $place) {
            try {
                $data = $this->placesApi->placeDetails($place->getPlaceId())->all();
                $this->places->updateDetails($place, $data);
                $io->writeln('[Updated] ' . $place . ' (' . $i . '/' . $c . ')');
            } catch (Exceptione $e) {
                $io->error($e->getMessage());
            }
            $i++;
        }
        $io->success(count($places) . ' places were updated');
    }
}
