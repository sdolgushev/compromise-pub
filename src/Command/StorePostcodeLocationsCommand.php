<?php

namespace App\Command;

use Exception;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Postcode;
use App\Entity\Place;
use App\Entity\PostcodeLocation;
use ExplorePlacesCommand;

class StorePostcodeLocationsCommand extends Command
{
    protected static $defaultName = 'app:store-postcode-locations';

    private $doctrine;
    private $postcodeHandler;

    public function __construct(ManagerRegistry $doctrine, Postcode $postcode) {
        $this->doctrine = $doctrine;
        $this->postcodeHandler = $postcode;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Extracts postcodes from existing places and converts them to locations')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limits the number of postcodes to convert and store', 100)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $limit = $input->getOption('limit');
        $counter = 0;

        $postcodes = $this->getPostcodesToStore($limit);
        if (count($postcodes) < $limit) {
            //$io->success('There are only ' . count($postcodes) . ' non-converted postcodes. They might be the invalid ones, so we are ignoring them.');
            return;
        }

        foreach ($postcodes as $postcode) {
            try {
                $this->postcodeHandler->getGeoPoints([$postcode]);
                $counter++;
            } catch (Exception $e) {
                $io->error($e->getMessage());
            }
        }

        $io->success($counter . ' postcode locations were stored');
    }

    private function getPostcodesToStore(int $limit): array
    {
        $postcodesToConvert = [];

        $repository = $this->doctrine->getRepository(PostcodeLocation::class);
        $places = $this->doctrine->getRepository(Place::class)->findAll();
        foreach ($places as $place) {
            $address = $place->getAddress();
            if (empty($address)) {
                continue;
            }

            $postcode = $this->postcodeHandler->extractPostcodeFromAddress($address);
            if ($postcode === null) {
                continue;
            }

            if ($repository->find($postcode) !== null) {
                continue;
            }

            if (in_array($postcode, $postcodesToConvert) === false) {
                $postcodesToConvert[] = $postcode;
            }

            if (count($postcodesToConvert) >= $limit) {
                break;
            }
        }

        return array_unique($postcodesToConvert);
    }
}
