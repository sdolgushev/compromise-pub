<?php

namespace App\Command;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Place;
use App\Command\ExplorePlacesCommand;
use App\Utils\GeoPoint;

class ResetPlacesToExploreCommand extends Command
{
    protected static $defaultName = 'app:reset-places-to-explore';

    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('If there are no places to explore, this script resets the furthest ones, so they will be explored next')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'How much places need to be reset', 100)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $limit = $input->getOption('limit');

        $repository = $this->doctrine->getRepository(Place::class);
        $manager = $this->doctrine->getManager();
        $placesToExplore = $repository->findToExplore();
        if (count($placesToExplore) > 0) {
            $io->success('There are still ' . count($placesToExplore) . ' places to explore. No need to reset anything.');
            return;
        }

        $coverage = new GeoPoint(
            ExplorePlacesCommand::EXPLORE_COVERAGE_CENTER_LAT,
            ExplorePlacesCommand::EXPLORE_COVERAGE_CENTER_LON
        );
        $placesToReset = $repository->findToReset($coverage, $limit);
        foreach ($placesToReset as $data) {
            $place = $data[0];
            $place->setExploredAt(null);
            $manager->merge($place);
            $manager->flush();
        }

        $io->success(count($placesToReset) . ' places were reset. We will explore them soon.');
    }
}
