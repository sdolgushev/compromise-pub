<?php

namespace App\Utils;

class GeoPoint
{
    private $lat = null;
    private $lon = null;

    public function __construct(float $lat = null, float $lon = null)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    public function getLat(): ?string
    {
        return self::formatFloat($this->lat);
    }

    public function getLon(): ?string
    {
        return self::formatFloat($this->lon);
    }

    public function isValid(): bool
    {
        if ($this->lat > 90 || $this->lat < -90) {
            return false;
        }

        if ($this->lon > 180 || $this->lon < -180) {
            return false;
        }

        return true;
    }

    public function __toString()
    {
        return 'Latitude: ' . $this->getLat() . ', longitude: ' . $this->getLon();
    }

    public function toArray()
    {
        return ['lat' => $this->getLat(), 'lon' => $this->getLon()];
    }

    public static function formatFloat(float $n): string
    {
        return (string) number_format($n, 7);
    }
}
