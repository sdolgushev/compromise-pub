<?php

namespace App\Repository;

use App\Entity\PostcodeLocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PostcodeLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostcodeLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostcodeLocation[]    findAll()
 * @method PostcodeLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostcodeLocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostcodeLocation::class);
    }

    public function autocomplete(string $postcode)
    {
        return $this->createQueryBuilder('p')
            ->orWhere('p.postcode LIKE :val')
            ->setParameter('val', $postcode . '%')
            ->orderBy('RAND()')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
