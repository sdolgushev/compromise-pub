<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Place;
use App\Utils\GeoPoint;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    // Meters
    const LOCATION_RADIUS = 500;

    const SORT_RATING_DESC = 'rating_desc';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findByLocation(
        GeoPoint $location,
        ?float $minRating = null,
        ?int $minUserRatingsTotal = null,
        ?array $priceRange = null,
        ?int $radius = null,
        ?int $limit = 10,
        ?string $sortBy = null
    ) {
        $q = $this->createQueryBuilder('l')
            ->select('l')
            ->addSelect(
                '( 6371000 * acos(cos(radians(' . $location->getLat() . '))' .
                '* cos( radians( l.lat ) )' .
                '* cos( radians( l.lon )' .
                '- radians(' . $location->getLon() . ') )' .
                '+ sin( radians(' . $location->getLat() . ') )' .
                '* sin( radians( l.lat ) ) ) ) as distance'
            )
            ->andWhere('l.was_deleted = 0')
            ->andWhere('l.permanently_closed = 0')
            ->andWhere('l.details_updated_at IS NOT NULL')
        ;

        // Rating filter
        if ($minRating > 0) {
            $q->andWhere('l.rating >= :min_rating')->setParameter('min_rating', $minRating * 100);
        }

        // User Rating Total filter
        if ($minUserRatingsTotal > 0) {
            $q->andWhere('l.user_ratings_total >= :min_user_ratings_total')->setParameter('min_user_ratings_total', $minUserRatingsTotal);
        }

        // Price range filters
        if (is_array($priceRange)) {
            if (isset($priceRange[0]) and Place::isValidPriceLevel($priceRange[0])) {
                $q->andWhere('l.price_level >= :min_price')->setParameter('min_price', (int)$priceRange[0]);
            }
            if (isset($priceRange[1]) and Place::isValidPriceLevel($priceRange[1])) {
                $q->andWhere('l.price_level <= :max_price')->setParameter('max_price', (int)$priceRange[1]);
            }
        }

        if ($radius === null) {
            $radius = self::LOCATION_RADIUS;
        }
        $q->having('distance < :distance')->setParameter('distance', $radius);

        if ($sortBy === null) {
            $q->orderBy('RAND()');
        } elseif ($sortBy === self::SORT_RATING_DESC) {
            $q->orderBy('l.rating', 'DESC');
        }

        return $q->setMaxResults($limit)->getQuery()->getResult();
    }

    public function findClosest(
        GeoPoint $location,
        float $minRating,
        array $priceRange
    ) {
        $q = $this->createQueryBuilder('l')
            ->select('l')
            ->addSelect(
                '( 6371000 * acos(cos(radians(' . $location->getLat() . '))' .
                '* cos( radians( l.lat ) )' .
                '* cos( radians( l.lon )' .
                '- radians(' . $location->getLon() . ') )' .
                '+ sin( radians(' . $location->getLat() . ') )' .
                '* sin( radians( l.lat ) ) ) ) as distance'
            )
            ->andWhere('l.was_deleted = 0')
            ->andWhere('l.permanently_closed = 0')
            ->andWhere('l.details_updated_at IS NOT NULL')
        ;

        // Rating filter
        if ($minRating > 0) {
            $q->andWhere('l.rating >= :min_rating')->setParameter('min_rating', $minRating * 100);
        }

        // Price range filters
        if (isset($priceRange[0]) and Place::isValidPriceLevel($priceRange[0])) {
            $q->andWhere('l.price_level >= :min_price')->setParameter('min_price', (int) $priceRange[0]);
        }
        if (isset($priceRange[1]) and Place::isValidPriceLevel($priceRange[1])) {
            $q->andWhere('l.price_level <= :max_price')->setParameter('max_price', (int) $priceRange[1]);
        }

        return $q->orderBy('distance', 'ASC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
        ;
    }

    public function findToExplore(string $placeId = null)
    {
        $q = $this->createQueryBuilder('p')
            ->andWhere('p.was_deleted = 0')
            ->andWhere('p.permanently_closed = 0')
            ->andWhere('p.explored_at IS NULL')
        ;

        if ($placeId !== null) {
            $q->andWhere('p.place_id = :place_id')->setParameter('place_id', $placeId);
        }

        return $q->getQuery()->getResult();
    }

    public function findToUpdate(string $placeId = null, int $limit = 10)
    {
        $q = $this->createQueryBuilder('p')
            ->andWhere('p.was_deleted = 0')
            ->andWhere('p.permanently_closed = 0')
            ->andWhere('p.details_updated_at IS NULL')
        ;

        if ($placeId !== null) {
            $q->andWhere('p.place_id = :place_id')->setParameter('place_id', $placeId);
        }

        return $q->getQuery()->setMaxResults($limit)->getResult();
    }

    public function findToReset(GeoPoint $location, int $limit) {
        return $this->createQueryBuilder('l')
            ->select('l')
            ->addSelect(
                '( 6371000 * acos(cos(radians(' . $location->getLat() . '))' .
                '* cos( radians( l.lat ) )' .
                '* cos( radians( l.lon )' .
                '- radians(' . $location->getLon() . ') )' .
                '+ sin( radians(' . $location->getLat() . ') )' .
                '* sin( radians( l.lat ) ) ) ) as distance'
            )
            ->andWhere('l.was_deleted = 0')
            ->andWhere('l.permanently_closed = 0')
            ->orderBy('distance', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        ;
    }
}
