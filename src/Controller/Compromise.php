<?php

namespace App\Controller;

use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Geo;
use App\Service\Postcode;
use App\Service\Places;
use App\Service\Texts;
use App\Repository\PlaceRepository;
use App\Entity\Place;
use App\Entity\PostcodeLocation;
use App\Utils\GeoPoint;

class Compromise extends AbstractController
{
    protected $texts = null;
    private $logger = null;

    public function __construct(Texts $texts, LoggerInterface $logger)
    {
        $this->texts = $texts;
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function homepage(Request $request, Postcode $postcodeHandler): Response
    {
        $input = $this->getInput($request, $postcodeHandler);

        $params = [
            'meta_title' => $this->texts->get('meta_title_homepage'),
            'friends' => $input['friends']['friends'],
            'min_rating' => $input['min_rating'],
            'price_range' => $input['price_range'],
            'price_range_formatted' => $input['price_range_formatted']
        ];
        return $this->render('homepage.html.twig', $params);
    }

    /**
     * @Route("/get_compromise_pub", name="get_compromise_pub")
     */
    public function getCompromisePub(
        Request $request,
        Postcode $postcodeHandler,
        Geo $geo,
        Places $places
    ): Response {
        // Handle input
        $input = $this->getInput($request, $postcodeHandler);
        $friends = $input['friends'];
        if (count($friends['errors']) > 0) {
            return $this->displayErrors($friends['errors'], $input);
        }
        $friends = $friends['friends'];
        if (count($friends) <= 1) {
            return $this->displayErrors([$this->texts->get('error_no_friends')], $input);
        }

        $friends = $this->setFriendsIdentifiers($friends);
        // Convert postcodes to geo points (locations)
        try {
            $friends = $this->convertPostcodesToGeoPoints($friends, $postcodeHandler);
        } catch (Exception $e) {
            $message = $this->texts->get('error_convert_postcodes_to_geopoints');
            return $this->displayErrors([$message], $input);
        }

        // Get centroid
        try {
            $centroid = $this->getCentroid($friends, $geo);
        } catch (Exception $e) {
            $this->logger->error('Unable to get centroid', $friends);
            return $this->displayErrors([$this->texts->get('error_get_centroid')], $input);
        }

        // Validate minimum distance
        try {
            $this->validateMinimumDistance($friends, $centroid, $geo, $postcodeHandler);
        } catch (Exception $e) {
            return $this->displayErrors([$e->getMessage()], $input);
        }

        // Get place
        $results = $this->getPlaces($places, $centroid, $input);
        if (count($results['items']) === 0) {
            return $this->displayErrors([$this->texts->get('error_get_place')], $input);
        }

        $input = $results['input'];
        $friends = $this->addDirectionsUrlToFriends($friends, $results['items']);
        $params = [
            'meta_title' => $this->texts->get('meta_title_map'),
            'messages' => $results['messages'],
            'places' => $results['items'],
            'place' => $results['items'][array_rand($results['items'])],
            'friends' => $friends,
            'min_rating' => $input['min_rating'],
            'price_range' => $input['price_range'],
            'price_range_formatted' => $input['price_range_formatted'],
            'params_string' => $this->getInputString($input),
            'centroid' => $centroid,
            'radius' => $results['radius'],
            'google_api_key' => getenv('GOOGLE_MAPS_API_KEY')
        ];
        return $this->render('compromise_pub.html.twig', $params);
    }

    public function texts(): Response
    {
        $params = [
            'meta_title' => $this->texts->get('meta_title_homepage'),
            'texts_list' => $this->texts->getAll()
        ];
        return $this->render('texts.html.twig', $params);
    }

    /**
     * @Route("/autocomplete/postcode/{postcode}", name="autocomplete_postcode")
     */
    public function autocompletePostcode(string $postcode, ManagerRegistry $doctrine): JsonResponse
    {
        $response = [
            'status' => 'OK',
            'suggestions' => []
        ];
        $postcodes = $doctrine->getRepository(PostcodeLocation::class)->autocomplete($postcode);
        foreach ($postcodes as $item) {
            $response['suggestions'][] = ['postcode' => strtoupper($item->getPostcode())];
        }

        return new JsonResponse($response);
    }

    private function getInput(Request $request, Postcode $postcodeHandler): array
    {
        $defaultMaxPriceLevel = 3;

        $priceRange = explode(',', $request->query->get('price_range', '0,' . $defaultMaxPriceLevel));
        $priceRangeFormatted = null;
        if (isset($priceRange[0])) {
            $priceRange[0] = (int) $priceRange[0];
            if (Place::isValidPriceLevel($priceRange[0])) {
                $priceRangeFormatted = Place::getPriceLabelForLevel(
                    $priceRange[0],
                    $this->texts->get('label_cheapest_price_level')
                );
            }
        }
        if (isset($priceRange[1])) {
            $priceRange[1] = (int) $priceRange[1];
            if ($priceRange[1] !== $priceRange[0] && Place::isValidPriceLevel($priceRange[1])) {
                $priceRangeFormatted .= ' - ' . Place::getPriceLabelForLevel($priceRange[1]);
            }
        } else {
            $priceRange[1] = $defaultMaxPriceLevel;
        }

        return [
            'min_rating' => (float) $request->query->get('min_rating', 4),
            'price_range' => $priceRange,
            'price_range_formatted' => $priceRangeFormatted,
            'friends' => $this->getFriends($request, $postcodeHandler),
        ];
    }

    private function getInputString(array $input): string
    {
        $return = 'min_rating='. (float) $input['min_rating']
            . '&price_range=' . implode(',', $input['price_range']);

        $friendsParams = [];
        foreach ($input['friends']['friends'] as $friend) {
            $friendsParams[] = $friend['name'] . ',' . $friend['postcode'];
        }
        $return .= '&friends=' . implode(';', $friendsParams);

        return $return;
    }

    private function getFriends(Request $request, Postcode $postcodeHandler): array
    {
        $friends = [];
        $errors = [];

        $friendsOptionalInput = $request->query->get('friends', null);
        if ($friendsOptionalInput !== null) {
            $names = [];
            $postcodes = [];

            $tmp = explode(';', $friendsOptionalInput);
            foreach ($tmp as $row) {
                $parts = explode(',', $row);
                if (count($parts) === 2) {
                    $names[] = $parts[0];
                    $postcodes[] = $parts[1];
                }
            }
        } else {
            $names = $request->query->get('names', []);
            $postcodes = $request->query->get('postcodes', []);
        }

        foreach ($names as $key => $name) {
            $name = trim($name);
            $postcode = isset($postcodes[$key]) ? trim($postcodes[$key]) : null;

            if (empty($name) && empty($postcodes[$key])) {
                $errors[] = $this->texts->get('error_missing_friends_name_and_postcode');
            } else {
                $params = ['name' => $name, 'postcode' => $postcode];
                if (empty($name)) {
                    $errors[] = $this->texts->get('error_missing_friends_name', $params);
                }

                if (empty($postcode)) {
                    $errors[] = $this->texts->get('error_missing_friends_postcode', $params);
                } elseif ($postcodeHandler->isValid($postcode) === false) {
                    $errors[] = $this->texts->get('error_invalid_friends_postcode', $params);
                }
            }

            $friends[] = [
                'name' => $name,
                'postcode' => $postcode
            ];
        }

        return ['friends' => $friends, 'errors' => $errors];
    }

    private function displayErrors(array $messages, array $input = [])
    {
        if (isset($input['friends']['friends'])) {
            $input['friends'] = $input['friends']['friends'];
        }

        $params = [
            'meta_title' => $this->texts->get('meta_title_homepage'),
            'errors' => $messages
        ] + $input;

        return $this->render('error.html.twig', $params);
    }

    private function setFriendsIdentifiers(array $friends): array
    {
        foreach ($friends as $i => $friend) {
            $friends[$i]['identifier'] = 'f' . md5(serialize($friend));
        }
        return $friends;
    }

    private function convertPostcodesToGeoPoints(array $friends, Postcode $postcodeHandler): array
    {
        $postcodes = [];
        foreach ($friends as $friend) {
            $postcodes[] = $friend['postcode'];
        }

        $geoPoints = $postcodeHandler->getGeoPoints($postcodes);
        if (count($geoPoints) !== count($friends)) {
            throw new Exception('Not all postcodes were converted');
        }

        foreach ($geoPoints as $i => $geoPoint) {
            $friends[$i]['location'] = $geoPoint;
        }

        return $friends;
    }

    private function getCentroid(array $friends, Geo $geo): GeoPoint {
        $geoPoints = [];
        foreach ($friends as $friend) {
            $geoPoints[] = $friend['location'];
        }

        return $geo->getCentroid($geoPoints);
    }

    private function validateMinimumDistance(
        array $friends,
        GeoPoint $centroid,
        Geo $geo,
        Postcode $postcodeHandler
    ): void {
        $minimalRadius = PlaceRepository::LOCATION_RADIUS;
        $everybodyIsTooClose = true;

        $postcodes = [];
        foreach ($friends as $friend) {
            $postcodes[] = $friend['postcode'];
            $distance = $geo->getDistance($friend['location'], $centroid);
            if ($distance >= $minimalRadius) {
                $everybodyIsTooClose = false;
            }
        }

        if ($everybodyIsTooClose) {
            $params = ['postcode' => $postcodeHandler->getCommonPostcode($postcodes)];
            throw new Exception($this->texts->get('error_minimal_friends_distance', $params));
        }
    }

    private function getPlaces(Places $places, GeoPoint $centroid, array $input): array
    {
        $return = [
            'radius' => PlaceRepository::LOCATION_RADIUS,
            'items' => [],
            'input' => [],
            'messages' => []
        ];
        $extendedParams = $this->getExtendedSearchParameters();

        // Try to increase radius
        for ($step = 1; $step <= 3; $step++) {
            try {
                $return['items'] = $places->get($centroid, $input['min_rating'], null, $input['price_range'], $return['radius']);
                break;
            } catch (Exception $e) {
                $return['radius'] += $extendedParams['radius_step'];

                if (isset($return['messages']['radius_increased']) === false) {
                    $message = $this->texts->get('search_notifications_radius_increased');
                    $return['messages']['radius_increased'] = $message;
                }
            }
        }

        // Try to downgrade rating and price range
        if (count($return['items']) === 0) {
            $canWeDowngradeFilters =
                $input['min_rating'] > $extendedParams['min_rating']
                || $input['price_range'][0] > $extendedParams['min_price_level']
                || $input['price_range'][1] < $extendedParams['max_price_level'];
            if ($canWeDowngradeFilters) {
                $input['min_rating'] = $extendedParams['min_rating'];
                $input['price_range'][0] = $extendedParams['min_price_level'];
                $input['price_range'][1] = $extendedParams['max_price_level'];

                $message = $this->texts->get('search_notifications_filters_downgraded');
                $return['messages']['filters_downgraded'] = $message;
            }

            try {
                $return['items'] = $places->get($centroid, $input['min_rating'], null, $input['price_range'], $return['radius']);
            } catch (Exception $e) {}
        }

        // Get one of the closest places
        if (count($return['items']) === 0) {
            try {
                $result = $places->getRandomClosest($centroid, $input['min_rating'], $input['price_range']);
                $return['items'] = [$result['item']];
                $return['radius'] = (int) ceil($result['distance']) + 10;
                $input['min_rating'] = $result['item']->getRating();
            } catch (Exception $e) {}
        }

        $return['input'] = $input;
        return $return;
    }

    private function getExtendedSearchParameters(): array
    {
        $minPriceLevel = Place::PRICE_LEVELS[0];
        $maxPriceLevel = array_values(array_slice(Place::PRICE_LEVELS, -1))[0];

        return [
            'radius_step' => 100,
            'min_rating' => 4.2,
            'min_price_level' => $minPriceLevel,
            'max_price_level' => $maxPriceLevel
        ];
    }

    private function addDirectionsUrlToFriends(array $friends, array $places): array
    {
        foreach ($friends as $i => $friend) {
            $friends[$i]['directions_urls'] = [];
            foreach ($places as $place) {
                $friends[$i]['directions_urls'][$place->getJsId()] = $place->getDirectionsUrl($friend['location']);
            }

        }

        return $friends;
    }
}
