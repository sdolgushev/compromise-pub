<?php

namespace App\Controller;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Command\ExplorePlacesCommand;
use App\Service\Texts;
use App\Service\Places;
use App\Repository\PlaceRepository;
use App\Utils\GeoPoint;

class Locator extends AbstractController
{
    const RADIUS_MIN = 100;
    const RADIUS_MAX = 1000;

    protected $texts = null;
    private $logger = null;

    public function __construct(Texts $texts, LoggerInterface $logger)
    {
        $this->texts = $texts;
        $this->logger = $logger;
    }

    /**
     * @Route("/locator", name="locator")
     */
    public function homepage(Request $request): Response
    {
        $input = $this->getInput($request);

        $params = [
            'meta_title' => $this->texts->get('meta_title_locator'),
            'input' => $input,
            'radius_limits' => ['min' => self::RADIUS_MIN, 'max' => self::RADIUS_MAX],
            'google_api_key' => getenv('GOOGLE_MAPS_API_KEY')
        ];
        return $this->render('locator.html.twig', $params);
    }

    /**
     * @Route("/locator_results", name="locator_results")
     */
    public function results(Request $request, Places $places): JsonResponse
    {
        $input = $this->getInput($request);
        $places = $this->getPlaces($input, $places);

        return new JsonResponse(['places' => $places]);
    }

    private function getInput(Request $request): array
    {
        $input = [
            'lat' => null,
            'lon' => null,
            'min_rating' => 3.6,
            'min_user_ratings_total' => 50,
            'radius' => 500,
            'default_location' => false
        ];

        foreach ($input as $var => $value) {
            if ($request->query->has($var)) {
                $input[$var] = $request->query->get($var);
            }
        }

        return $this->validateInput($input);
    }

    private function validateInput(array $input): array
    {
        if ($input['lat'] === null || $input['lon'] === null) {
            $input['lat'] = ExplorePlacesCommand::EXPLORE_COVERAGE_CENTER_LAT;
            $input['lon'] = ExplorePlacesCommand::EXPLORE_COVERAGE_CENTER_LON;
            $input['default_location'] = true;
        }

        if ($input['min_rating'] < 0) {
            $input['min_rating'] = 0;
        } elseif ($input['min_rating'] > 5) {
            $input['min_rating'] = 5;
        }

        if ((int) $input['min_user_ratings_total'] <= 0) {
            $input['min_user_ratings_total'] = 0;
        }

        if ($input['radius'] < self::RADIUS_MIN) {
            $input['radius'] = self::RADIUS_MIN;
        } elseif ($input['radius'] > self::RADIUS_MAX) {
            $input['radius'] = self::RADIUS_MAX;
        }

        return $input;
    }

    private function getPlaces(array $input, Places $places): array
    {
        $return = [];
        try {
            $places = $places->get(
                new GeoPoint($input['lat'], $input['lon']),
                $input['min_rating'],
                $input['min_user_ratings_total'],
                null,
                $input['radius'],
                20,
                PlaceRepository::SORT_RATING_DESC
            );
        } catch(Exception $e) {
            return $return;
        }

        foreach ($places as $place) {
            $return[] = [
                'id' => $place->getJsId(),
                'place_id' => $place->getPlaceId(),
                'name' => $place->getName(),
                'lat' => $place->getLat(),
                'lon' => $place->getLon(),
                'rating' => $place->getRating(),
                'user_ratings_total' => $place->getUserRatingsTotal(),
                'price_level' => $place->getPriceLevelLabel(),
                'reviews_url' => $place->getReviewsUrl(),
                'url' => $place->getUrl(),
                'address' => $place->getAddress(),
                'directions_url' => $place->getDirectionsUrl(),
                'website' => $place->getWebsite(),
                'website_name' => $place->getWebsiteName()
            ];
        }

        return $return;
    }
}
