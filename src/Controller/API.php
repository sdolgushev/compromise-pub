<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Geo;
use App\Service\Postcode;
use App\Service\Places;
use Exception;

class API extends AbstractController
{
    protected $geo = null;

    private function __construct(Geo $geo)
    {
        $this->geo = $geo;
    }

    /**
     * @Route(
     *     "/get_centroid/{points}",
     *     name="get_centroid",
     *     methods={"GET"},
     *     requirements={"points"="[\-0-9\.\,\/]+"}
     * )
     */
    public function getCentriod(string $points): JsonResponse
    {
        $points = $this->geo->parsePoints($points);
        return $this->getCentroidForPoints($points);
    }

    /**
     * @Route(
     *     "/get_postcodes_centroid/{postcodes}",
     *     name="get_postcodes_centroid",
     *     methods={"GET"},
     *     requirements={"postcodes"="[ 0-9a-zA-Z\/]+"}
     * )
     */
    public function getCentriodByPostcodes(string $postcodes, Postcode $handler): JsonResponse
    {
        $postcodes = $handler->parsePostcodes($postcodes);
        if (count($postcodes) < 2) {
            return $this->error('At least two valid postcodes are required');
        }

        try {
            $points = $handler->getGeoPoints($postcodes);
        } catch (Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->getCentroidForPoints($points);
    }

    /**
     * @Route(
     *     "/get_place/{folks}",
     *     name="get_places",
     *     methods={"GET"},
     *     requirements={"postcodes"="[ 0-9a-zA-Z\/]+"}
     * )
     */
    public function getPlacesForPostcode(
        string $postcode,
        Postcode $postCodeHandler,
        Places $places
    ): JsonResponse {
        if ($postCodeHandler->isValid($postcode) === false) {
            return $this->error('Invalid postcode');
        }

        try {
            $points = $postCodeHandler->getGeoPoints([$postcode]);
        } catch (Exception $e) {
            return $this->error($e->getMessage());
        }

        $place = $places->get($points[0]);
        var_dump($place); exit();
    }

    private function getCentroidForPoints(array $points): JsonResponse
    {
        if (count($points) < 2) {
            return $this->error('At least two valid points are required');
        }

        try {
            $centroid = $this->geo->getCentroid($points);
        } catch (Exception $e) {
            return $this->error($e->getMessage());
        }

        return $this->response($centroid->toArray());
    }

    private function error($message): JsonResponse
    {
        return $this->response(null, $message);
    }

    private function response($data, $error = null): JsonResponse
    {
        return $this->json(['data' => $data, 'error' => $error]);
    }
}
