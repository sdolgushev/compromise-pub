<?php

namespace App\Service;

use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\PostcodeLocation;
use App\Utils\GeoPoint;

class Postcode
{
    # https://en.wikipedia.org/wiki/London_postal_district
    private static $allowedPrefixes = [
        // E
        'e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'e10', 'e11', 'e12', 'e13', 'e14', 'e15', 'e16', 'e17', 'e18', 'e20',
        // EC
        'ec1', 'ec2', 'ec3', 'ec4',
        // N
        'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'n10', 'n11', 'n12', 'n13', 'n14', 'n15', 'n16', 'n17', 'n18', 'n19', 'n20', 'n21', 'n22',
        // NW
        'nw1', 'nw2', 'nw3', 'nw4', 'nw5', 'nw6', 'nw7', 'nw8', 'nw9', 'nw10', 'nw11',
        // SE
        'se1', 'se2', 'se3', 'se4', 'se5', 'se6', 'se7', 'se8', 'se9', 'se10', 'se11', 'se12', 'se13', 'se14', 'se15', 'se16', 'se17', 'se18', 'se19', 'se20', 'se21', 'se22', 'se23', 'se24', 'se25', 'se26', 'se27', 'se28',
        // SW
        'sw1', 'sw2', 'sw3', 'sw4', 'sw5', 'sw6', 'sw7', 'sw8', 'sw9', 'sw10', 'sw11', 'sw12', 'sw13', 'sw14', 'sw15', 'sw16', 'sw17', 'sw18', 'sw19', 'sw20',
        // W
        'w1', 'w2', 'w3', 'w4', 'w5', 'w6', 'w7', 'w8', 'w9', 'w10', 'w11', 'w12', 'w13', 'w14',
        // WC
        'wc1', 'wc2',
        // Around London
        'wd', 'en', 'cm', 'ig', 'rm', 'da', 'br', 'cr', 'sm', 'kt', 'tw', 'ub', 'ha'
    ];
    private static $permittedLetters = [
        1 => "[abcdefghijklmnoprstuwyz]",
        2 => "[abcdefghklmnopqrstuvwxy]",
        3 => "[abcdefghjkpmnrstuvwxy]",
        4 => "[abehmnprvwxy]",
        5 => "[abdefghjlnpqrstuwxyz]"
    ];

    private $logger = null;
    private $doctrine = null;

    public function __construct(LoggerInterface $logger, ManagerRegistry $doctrine)
    {
        $this->logger = $logger;
        $this->doctrine = $doctrine;
    }

    public function parsePostcodes(string $postcodes): array
    {
        $return = [];

        $postcodes = explode('/', $postcodes);
        foreach ($postcodes as $postcode) {
            if ($this->isValid($postcode) === false) {
                continue;
            }

            $return[] = $postcode;
        }

        return $return;
    }

    public function isValid(string $postcode): bool
    {
        $postcode = strtolower($postcode);
        if ($this->validatePrefix($postcode) === false) {
            return false;
        }

        $regExps = $this->getValidationRegExps();
        foreach ($regExps as $regExp) {
            if (preg_match($regExp, $postcode, $matches)) {
                return true;
            }
        }

        return false;
    }

    public function getGeoPoints(array $postcodes): array
    {
        $data = $this->fetchStoredGeoPoints($postcodes);

        if (count($data['postcodes'])) {
            $data['geo_points'] = array_merge(
                $data['geo_points'],
                $this->convertPostcodesToGeoPoints($data['postcodes'])
            );
        }

        return $data['geo_points'];
    }

    public function getPostcodeByGeoPoint(GeoPoint $geoPoint): string
    {
        $url = 'https://api.postcodes.io/postcodes';
        $url .= '?lon=' . $geoPoint->getLon() . '&lat=' . $geoPoint->getLat() . '';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        $data = json_decode($response, true);
        if (is_array($data) === false || isset($data['result']) === false) {
            throw new Exception('Got wrong response from postcodes API');
        }

        return $data['result'][0]['postcode'];
    }

    public function extractPostcodeFromAddress(string $address): ?string
    {
        $tmp = str_replace(', UK', '', $address);
        $parts = explode(' ', $tmp);
        if (count($parts) < 2) {
            return null;
        }

        $postcodeParts = array_slice($parts, -2);
        $postcode = implode(' ', $postcodeParts);

        return $this->isValid($postcode) ? $postcode : null;
    }

    public function getCommonPostcode(array $postcodes): string
    {
        foreach ($postcodes as $k => $postcode) {
            $postcodes[$k] = strtoupper($postcode);
        }

        $common = '';
        $defaultPostcodeChars = str_split($postcodes[0]);
        $otherPostcodes = array_slice($postcodes, 1);
        foreach ($defaultPostcodeChars as $i => $char) {
            foreach ($otherPostcodes as $otherPostcode) {
                if ($otherPostcode[$i] !== $char) {
                    break 2;
                }

                $common .= $char;
            }
        }

        return $common;
    }

    private function fetchStoredGeoPoints(array $postcodes): array
    {
        $return = ['geo_points' => [], 'postcodes' => $postcodes];

        foreach ($return['postcodes'] as $i => $postcode) {
            $geoPoint = $this->fetchGeoPointByPostcode($postcode);
            if ($geoPoint !== null) {
                $return['geo_points'][] = $geoPoint;
                unset($return['postcodes'][$i]);
            }
        }

        return $return;
    }

    private function fetchGeoPointByPostcode($postcode): ?GeoPoint
    {
        $repo = $this->doctrine->getRepository(PostcodeLocation::class);
        $pl = $repo->find($postcode);

        return $pl !== null ? new GeoPoint($pl->getLatitude(), $pl->getLongitude()) : null;
    }

    private function convertPostcodesToGeoPoints(array $postcodes): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.postcodes.io/postcodes');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['postcodes' => $postcodes]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        $data = json_decode($response, true);
        if (is_array($data) === false || isset($data['result']) === false) {
            throw new Exception('Got wrong response from postcodes API');
        }

        $geoPoints = [];
        foreach ($data['result'] as $item) {
            if ($item['result'] === null) {
                $this->logger->error('Unable to get get location', ['postcode' => $item['query']]);
                throw new Exception('Unable to get get location for "' . $item['query'] . '" postcode');
            }

            $geoPoint = new GeoPoint($item['result']['latitude'], $item['result']['longitude']);
            $this->storePostcodeLocation($item['query'], $geoPoint);
            $geoPoints[] = $geoPoint;
        }

        return $geoPoints;
    }

    private function storePostcodeLocation($postcode, GeoPoint $geoPoint): PostcodeLocation
    {
        $pl = new PostcodeLocation();
        $pl->setPostcode($postcode);
        $pl->setLatitude($geoPoint->getLat());
        $pl->setLongitude($geoPoint->getLon());
        $em = $this->doctrine->getManager();
        $em->persist($pl);
        $em->flush();
        return $pl;
    }

    private function validatePrefix(string $postcode): bool
    {
        foreach (self::$allowedPrefixes as $prefix) {
            if (strpos($postcode, $prefix) === 0) {
                return true;
            }
        }

        return false;
    }

    private function getValidationRegExps(): array
    {
        return array(
            // Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA with a space
            '/^('.self::$permittedLetters[1].'{1}'.self::$permittedLetters[2].'{0,1}[0-9]{1,2})([[:space:]]{0,})([0-9]{1}'.self::$permittedLetters[5].'{2})$/',
            // Expression for postcodes: ANA NAA
            '/^('.self::$permittedLetters[1].'{1}[0-9]{1}'.self::$permittedLetters[3].'{1})([[:space:]]{0,})([0-9]{1}'.self::$permittedLetters[5].'{2})$/',
            // Expression for postcodes: AANA NAA
            '/^('.self::$permittedLetters[1].'{1}'.self::$permittedLetters[2].'{1}[0-9]{1}'.self::$permittedLetters[4].')([[:space:]]{0,})([0-9]{1}'.self::$permittedLetters[5].'{2})$/'
        );
    }
}
