<?php

namespace App\Service;

use DateTime;
use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use SKAgarwal\GoogleApi\PlacesApi;
use App\Utils\GeoPoint;
use App\Entity\Place;

class Places
{
    private $placesApi = null;
    private $logger = null;
    private $doctrine = null;

    public function __construct(
        PlacesApi $placesApi,
        LoggerInterface $logger,
        ManagerRegistry $doctrine
    ) {
        $this->placesApi = $placesApi;
        $this->logger = $logger;
        $this->doctrine = $doctrine;
    }

    public function get(
        GeoPoint $point,
        ?float $minRating = null,
        ?int $minUserRatingsTotal = null,
        ?array $priceRange = null,
        ?int $radius = null,
        ?int $limit = 10,
        ?string $sortBy = null
    ): array {
        $repo = $this->doctrine->getRepository(Place::class);
        $result = $repo->findByLocation($point, $minRating, $minUserRatingsTotal, $priceRange, $radius, $limit, $sortBy);

        $places = [];
        foreach ($result as $item) {
            $places[] = $item[0];
        }

        if (count($places) === 0) {
            throw new Exception('Places not found');
        }

        return $places;
    }

    public function getRandomClosest(
        GeoPoint $point,
        float $minRating,
        array $priceRange
    ): array {
        $repo = $this->doctrine->getRepository(Place::class);
        $result = $repo->findClosest($point, $minRating, $priceRange);

        if (count($result) === 0) {
            throw new Exception('Places not found');
        }
        $result = $result[array_rand($result)];

        return [
            'item' => $result[0],
            'distance' => $result['distance']
        ];
    }

    public function findPlace(string $id)
    {
        return $this->doctrine->getRepository(Place::class)->find($id);
    }

    public function storePlaceGeneralInfo(array $data): Place
    {
        $place = new Place();
        $place->setId($data['id']);
        $place->setPlaceId($data['place_id']);
        $place->setCreatedAt(new DateTime());

        $place->setName($data['name']);
        $place->setLat($data['geometry']['location']['lat']);
        $place->setLon($data['geometry']['location']['lng']);
        $place->setTypes($data['types']);
        if (isset($data['rating'])) {
            $place->setRating($data['rating']);
        }
        if (isset($data['user_ratings_total'])) {
            $place->setUserRatingsTotal($data['user_ratings_total']);
        }
        if (isset($data['price_level'])) {
            $place->setPriceLevel($data['price_level']);
        }
        $place->setIconUrl($data['icon']);

        $em = $this->doctrine->getManager();
        $em->persist($place);
        $em->flush();

        return $place;
    }

    public function updateDetails(Place $place, array $data): Place
    {
        $place->setDetailsUpdatedAt(new DateTime());

        $deletedStatuses = ['ZERO_RESULTS', 'NOT_FOUND'];
        if (isset($data['status']) && in_array($data['status'], $deletedStatuses)) {
            $place->setWasDeleted(true);
        }

        $okStatuses = ['OK'];
        if (isset($data['status']) && in_array($data['status'], $okStatuses)) {
            $fields = $data['result'];

            $place->setAddress($fields['formatted_address']);
            $place->setUrl($fields['url']);

            if (isset($fields['formatted_phone_number'])) {
                $place->setPhoneNumber($fields['formatted_phone_number']);
            }
            if (isset($fields['website'])) {
                $place->setWebsite($fields['website']);
            }
            if (isset($fields['rating'])) {
                $place->setRating($fields['rating']);
            }
            if (isset($fields['user_ratings_total'])) {
                $place->setUserRatingsTotal($fields['user_ratings_total']);
            }
            if (isset($fields['opening_hours'])) {
                $place->setOpeningHours($fields['opening_hours']);
            }
            if (isset($fields['permanently_closed'])) {
                $place->setPermanentlyClosed(true);
            }
        }

        $em = $this->doctrine->getManager();
        $em->merge($place);
        $em->flush();

        return $place;
    }
}