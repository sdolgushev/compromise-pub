<?php

namespace App\Service;

class Texts
{
    private static $texts = [
        // Meta
        'meta_title_homepage' => 'Find Compromise Pub in London for your crowd | Search',
        'meta_title_map' => 'Find Compromise Pub in London for your crowd  | Search Results',
        'meta_title_locator' => 'Find Compromise Pub in London for your crowd  | Pubs Locator',
        'meta_description' => 'Need to find the best pub in London for your crowd? This is the perfect place to look for one! Just relax and be prepared to have a great time with your mates. We will find the Compromise Pub for you!',
        'meta_og_site_name' => 'Find Compromise Pub in London for your crowd',
        'meta_twitter_title' => 'Find Compromise Pub in London for your crowd',
        'meta_twitter_description' => 'Need to find the best pub in London for your crowd? Just relax and be prepared to have a great time! We will do the rest!',
        'intro_text' =>
            '<p>You want to have a few pints with folks. You start to pick a pub. After hours of negotiations (nobody wants to go an extra mile) you realize it is a disaster. As a result you end up with a bottle of wine on your comfy sofa.</p>
<p>If you have ever been in a case like that then this is a lifesaver for you. We will help you to find a Compromise Pub for your crowd. Plus, as a bonus, you will have a spare bottle of wine.</p>
<p>Just provide us with the postcodes you know and we will do the rest!</p>',
        // Friends form
        'friends_form_label_min_rating' => 'How picky are you?',
        'friends_form_label_price_range' => 'How much you want to spend?',
        'friends_form_label_friends' => 'Mates',
        'friends_form_label_add' => 'More',
        'friends_form_label_remove_friend' => 'Is not coming',
        'friends_form_button_search' => 'Let\'s Get Ready To Rumble!',
        // Locator
        'locator_label_min_rating' => 'Minimal rating',
        'locator_label_min_user_ratings_total' => 'Minimal user ratings total',
        'locator_label_radius' => 'Radius (meters)',
        // Errors
        'error_friends_header' => 'Wait a second! We need to sort out some things first',
        'error_missing_friends_name_and_postcode' => 'We can not really help if you will not provide people\'s names and postcodes. Our magic is not strong enough.',
        'error_missing_friends_name' => 'Forgot a name of your fellow from "%postcode%"? Do you really want to find some compromise with her/him?',
        'error_missing_friends_postcode' => 'Do you like %name%? Even though if not, please provide his/her postcode!',
        'error_invalid_friends_postcode' => 'Hope you did not mail any postcards to %name% because their "%postcode%" does not seem to be valid.',
        'error_no_friends' => 'Are you going to find a Compromise Pub just for yourself? Don\'t be selfish! Invite some friends!',
        'error_convert_postcodes_to_geopoints' => 'Nobody is perfect. Neither our robots are. They had some troubles with "%postcode%" postcode. Can you please use the closest one?',
        'error_get_centroid' => 'One of our magic spells is broken, cannot find compromise location for your postcodes. Don\'t you want to go for a nice hike with your friends?',
        'error_get_place' => 'Compromise area was found for you. But we were unable to find any pubs there. It\'s unbelievable there are still some places in London without proper pubs. Most likely you are just too picky?',
        'error_minimal_friends_distance' => 'It would be better for everybody if you just go to some local pub with your fellows. Come back here after you will invite some mates outside %postcode%.',
        // Search notifications
        'search_notifications_header' => 'Oh dear! Our default spells didn\'t work out, so it required some extra effort from us:',
        'search_notifications_radius_increased' => 'The extended search area was used',
        'search_notifications_filters_downgraded' => 'You might spend more or get less fancy place. But is all about finding the compromise. Right?',
        // Logo
        'logo_title' => 'Compromise Pub',
        'logo_motto' => 'Solving the most important problems. Indeed.',
        // Banners
        'banner_label' => [
            'We need to make our millions somehow, right?',
            'Would you prefer to buy us a pint, instead of clicking this banner?',
            'If I were you, I would click this banner ... at least 3 times',
            'This advert says about your personality more than your mother can say',
            'We need to cover our Google API expenses... If you are not a web developer, just click it and we will be fine. Thanks.'
        ],
        // Labels
        'label_cheapest_price_level' => 'Most affordable',
        'label_get_next_compromise_pub' => 'Is there anything else?',
        'label_change_parameters' => '&larr; Wanna make some corrections',
        // Footer
        'footer_note' => 'Do you have any suggestions or do you want to let us know how this service improved the quality of your life on Friday nights? Then just <a href="mailto:compromise.pub@therapist.net?Subject=Would%20like%20to%20say%20how%20brilliant%20you%20are" data-gtm="footer-notes"         rel="nofollow">send us</a> a few warm words!',
        'copyright' => '&copy; %year% <a href="mailto:compromise.pub@therapist.net
?Subject=Would%20like%20to%20say%20how%20brilliant%20you%20are" data-gtm="copyright"         rel="nofollow">Compromise Pub<a/>',
    ];

    public function get(string $identifier, array $params = []): string
    {
        $text = self::$texts[$identifier];
        if (is_array($text)) {
            $text = $text[array_rand($text)];
        }

        foreach ($params as $key => $value) {
            $text = str_replace('%' . $key . '%', $value, $text);
        }

        return $text;
    }

    public function getAll(): array
    {
        return self::$texts;
    }
}
