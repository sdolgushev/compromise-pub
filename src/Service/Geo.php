<?php

namespace App\Service;

use Exception;
use App\Utils\GeoPoint;

class Geo
{
    public function __construct() {}

    public function parsePoints(string $points): array {
        $items = explode('/', $points);
        $points = [];

        foreach ($items as $item) {
            $tmp = explode(',', $item);
            if (count($tmp) !== 2) {
                continue;
            }

            $point = new GeoPoint($tmp[0], $tmp[1]);
            if ($point->isValid()) {
                $points[] = $point;
            }
        }

        return $points;
    }

    public function getCentroid($points): GeoPoint
    {
        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        $PI = pi();
        foreach ($points as $point) {
            if ($point->isValid() === false) {
                throw new Exception('Invalid point: ' . $point);
            }

            $lat = $point->getLat() * $PI / 180;
            $lon = $point->getLon() * $PI / 180;

            $a = cos($lat) * cos($lon);
            $b = cos($lat) * sin($lon);
            $c = sin($lat);

            $X += $a;
            $Y += $b;
            $Z += $c;
        }

        $count = count($points);
        $X /= $count;
        $Y /= $count;
        $Z /= $count;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        return new GeoPoint($lat * 180 / $PI, $lon * 180 / $PI);
    }

    public function getDistance(GeoPoint $a, GeoPoint $b, int $earthRadius = 6371000): int
    {
        $latFrom = deg2rad($a->getLat());
        $lonFrom = deg2rad($a->getLon());

        $latTo = deg2rad($b->getLat());
        $lonTo = deg2rad($b->getLon());

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }
}
