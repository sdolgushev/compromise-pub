<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Utils\GeoPoint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 */
class Place
{
    const PRICE_LEVELS = [0, 1, 2, 3, 4];
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $explored_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @ORM\Column(type="float")
     */
    private $lon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $types;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_ratings_total;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price_level = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon_url;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $details_updated_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $opening_hours;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permanently_closed = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="boolean")
     */
    private $was_deleted = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getExploredAt(): ?\DateTimeInterface
    {
        return $this->explored_at;
    }

    public function setExploredAt(?\DateTimeInterface $explored_at): self
    {
        $this->explored_at = $explored_at;

        return $this;
    }

    public function getPlaceId(): ?string
    {
        return $this->place_id;
    }

    public function setPlaceId(string $place_id): self
    {
        $this->place_id = $place_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLat(): ?string
    {
        return GeoPoint::formatFloat($this->lat);
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?string
    {
        return GeoPoint::formatFloat($this->lon);
    }

    public function setLon(float $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    public function getTypes(): ?array
    {
        return empty($this->types) ? [] : explode(',', $this->types);
    }

    public function setTypes(?array $types): self
    {
        $this->types = implode(',', $types);

        return $this;
    }

    public function getRating(): ?float
    {
        return $this->rating / 100;
    }

    public function setRating(?float $rating): self
    {
        $this->rating = $rating  * 100;

        return $this;
    }

    public function getUserRatingsTotal(): ?int
    {
        return $this->user_ratings_total;
    }

    public function setUserRatingsTotal(?int $user_ratings_total): self
    {
        $this->user_ratings_total = $user_ratings_total;

        return $this;
    }

    public function getReviewsUrl(): string
    {
        return 'https://search.google.com/local/reviews?placeid=' . $this->getPlaceId();
    }

    public function getPriceLevel(): ?int
    {
        return $this->price_level;
    }

    public function setPriceLevel(?int $price_level): self
    {
        $this->price_level = $price_level;

        return $this;
    }

    public function getPriceLevelLabel(): ?string
    {
        return self::getPriceLabelForLevel($this->getPriceLevel());
    }

    public function getIconUrl(): ?string
    {
        return $this->icon_url;
    }

    public function setIconUrl(?string $icon_url): self
    {
        $this->icon_url = $icon_url;

        return $this;
    }

    public function getDetailsUpdatedAt(): ?\DateTimeInterface
    {
        return $this->details_updated_at;
    }

    public function setDetailsUpdatedAt(?\DateTimeInterface $details_updated_at): self
    {
        $this->details_updated_at = $details_updated_at;

        return $this;
    }

    public function getOpeningHours(): ?array
    {
        return json_decode($this->opening_hours, true);
    }

    public function setOpeningHours(array $opening_hours): self
    {
        $this->opening_hours = json_encode($opening_hours);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPermanentlyClosed(): ?bool
    {
        return $this->permanently_closed;
    }

    public function setPermanentlyClosed(bool $permanently_closed): self
    {
        $this->permanently_closed = $permanently_closed;

        return $this;
    }

    public function getUrl(): ?string
    {
        $defaultUrl = 'https://www.google.com/maps/place/?q=place_id:' . $this->getPlaceId();
        return empty($this->url) ? $defaultUrl : $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getWasDeleted(): ?bool
    {
        return $this->was_deleted;
    }

    public function setWasDeleted(bool $was_deleted): self
    {
        $this->was_deleted = $was_deleted;

        return $this;
    }

    public function getDirectionsUrl(GeoPoint $from = null): string
    {
        $url = 'https://citymapper.com/directions?endcoord=' . $this->getLat()
            . ',' . $this->getLon() . '&endname=' . $this->getName();

        $address = $this->getAddress();
        if (empty($address) === false) {
            $url .= '&endaddress=' . $this->getAddress();
        }

        if ($from instanceof GeoPoint) {
            $url .= '&startcoord=' . $from->getLat() . ',' . $from->getLon();
        }

        return $url;
    }

    public function getWebsiteName(): ?string
    {
        return parse_url($this->getWebsite(), PHP_URL_HOST);
    }

    public function getJsId(): ?string
    {
        return 'p' . $this->id;
    }

    public function __toString()
    {
        return '"' . $this->getName() . '" #' . $this->getPlaceId() . ' [lat: ' . $this->getLat() . ', lon: ' . $this->getLon() . ']';
    }

    public static function isValidPriceLevel(int $level): bool
    {
        return in_array($level, self::PRICE_LEVELS);
    }

    public static function getPriceLabelForLevel(int $level, string $placeholder = null): string
    {
        $label = str_repeat('£', $level);

        if (empty($label) && $placeholder !== null) {
            $label = $placeholder;
        }

        return $label;
    }
}
