<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190127041359 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE postcode_location DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE postcode_location CHANGE postalcode postcode VARCHAR(8) NOT NULL');
        $this->addSql('ALTER TABLE postcode_location ADD PRIMARY KEY (postcode)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE postcode_location DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE postcode_location CHANGE postcode postalcode VARCHAR(8) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE postcode_location ADD PRIMARY KEY (postalcode)');
    }
}
