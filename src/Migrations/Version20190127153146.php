<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190127153146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE place (id VARCHAR(255) NOT NULL, explored_at DATETIME DEFAULT NULL, place_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, lat DOUBLE PRECISION NOT NULL, lon DOUBLE PRECISION NOT NULL, types VARCHAR(255) DEFAULT NULL, rating INT DEFAULT NULL, user_ratings_total INT DEFAULT NULL, price_level INT DEFAULT NULL, icon_url VARCHAR(255) DEFAULT NULL, details_updated_at DATETIME DEFAULT NULL, opening_hours LONGTEXT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, permanently_closed TINYINT(1) NOT NULL, url VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, was_deleted TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE place');
    }
}
