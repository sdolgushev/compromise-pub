const $ = require('jquery');

var compromiseMap = {
    elements: {
        map: null,
        pan_to_compromise_pub: null,
        get_next_compromise_pub: null,
        friends_links: null
    },
    pub: null,
    pubs: {},
    friends: {},
    area: {},
    pub_marker: null,
    pub_infowindow: null,
    friend_markers: {},
    friend_infowindows: {},
    shown_pubs: [],
    map: null,
    init: function() {
        this.map = new google.maps.Map(this.elements.map[0], {
            center: this.pub.location,
            zoom: 12,
            disableDefaultUI: true,
            fullscreenControl: true
        });
    },
    showPubMarker: function() {
        var p = this.pub;
        var marker = new google.maps.Marker({
            position: p.location,
            map: this.map,
            title: p.title,
            icon: new google.maps.MarkerImage(
                p.icon,
                new google.maps.Size(71, 71),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 26),
                new google.maps.Size(25, 25)
            ),
            zIndex: 999
        });

        var infowindow = new google.maps.InfoWindow({
            content: this.getPubInfoWindowContent(),
            pixelOffset: new google.maps.Size(-23, 0)
        });
        marker.addListener('click', function() {
            infowindow.open(marker.get('map'), marker);
        });
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) === false ) {
            infowindow.open(marker.get('map'), marker);
        }

        this.pub_infowindow = infowindow;
        this.pub_marker = marker;
    },
    getPubInfoWindowContent: function() {
        var p = this.pub;
        var content = '<h5><a href="' + p.url + '" target="_blank" data-gtm="pub-info-title">' + p.title + '</a></h5>';
        if (p.address) {
            content += '<a href="' + p.directions_url + '" target="_blank" data-gtm="directions-pub">' + p.address + '</a><br />';
        }
        if (p.rating) {
            var priceLevel = '';
            if (p.price_level) {
                priceLevel = ', <span>' + p.price_level + '</span>';
            }
            content += 'Raiting: ' + p.rating + ', based on <a href="' + p.reviews_url + '" target="_blank" data-gtm="pub-info-reviews">' + p.user_ratings_total + '</a> reviews' + priceLevel;
        }
        if (p.website) {
            content += '<br /><a href="' + p.website + '" target="_blank" data-gtm="pub-info-website">' + p.website_name + '</a>';
        }
        return content;
    },
    showFriends: function() {
        for (var identifier in this.friends) {
            if (this.friends.hasOwnProperty(identifier)) {
                this.addFriendMarker(this.friends[identifier]);
            }
        }
    },
    addFriendMarker: function(friend) {
        var marker = new google.maps.Marker({
            position: friend.location,
            map: this.map,
            title: friend.name,
            label: friend.name.charAt(0)
        });

        var infowindow = new google.maps.InfoWindow({ content: this.getFriendInfoWindowContent(friend) });
        marker.addListener('click', function() {
            infowindow.open(marker.get('map'), marker);
        });

        this.friend_markers[friend.id] = marker;
        this.friend_infowindows[friend.id] = infowindow;
    },
    getFriendInfoWindowContent: function(friend) {
        var content = '<h5>' + friend.name + '</h5>';
        content += '<a href="' + friend.directions_urls[this.pub.id] + '" target="_blank" data-gtm="directions-friend">' + friend.postcode + ' to ' + this.pub.title + '</a>';
        return content;
    },
    setZoom: function () {
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(this.pub.location);
        for (var identifier in this.friends) {
            if (this.friends.hasOwnProperty(identifier)) {
                bounds.extend(this.friends[identifier].location);
            }
        }
        this.map.fitBounds(bounds);
    },
    showArea: function() {
        new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.25,
            strokeWeight: 1,
            fillColor: '#FF0000',
            fillOpacity: 0.05,
            map: this.map,
            center: this.area.location,
            radius: this.area.radius
        });
    },
    installPunToPubMarkerHandler: function() {
        var that = this;
        this.elements.pan_to_compromise_pub.on('click', function(e) {
            e.preventDefault();
            new google.maps.event.trigger(that.pub_marker, 'click');
            that.map.panTo(that.pub_marker.position);
        });
    },
    installGetNextPubHandler: function() {
        var that = this;
        this.elements.get_next_compromise_pub.on('click', function(e) {
            e.preventDefault();
            that.updateNextPub();
        });
    },
    installFriendsLinksHandler: function() {
        var that = this;
        this.elements.friends_links.on('click', function(e) {
            e.preventDefault();
            var identifier = $(this).attr('id').replace('friend_', '');
            if(
                typeof(that.friend_markers[identifier]) != "undefined"
                && that.friend_markers[identifier] !== null
            ) {
                that.clickOnMarker(that.friend_markers[identifier]);
            }
        });
    },
    getAnotherRandomPub: function() {
        if (Object.keys(this.shown_pubs).length === Object.keys(this.pubs).length) {
            this.shown_pubs = [this.pub.id];
        }

        var possiblePubIds = [];
        for (var id in this.pubs) {
            if (this.pubs.hasOwnProperty(id) === false) {
                continue;
            }
            if (this.shown_pubs.includes(id)) {
                continue;
            }

            possiblePubIds.push(id);
        }

        var randomPubId = possiblePubIds[Math.floor(Math.random()*possiblePubIds.length)];
        this.shown_pubs.push(randomPubId);
        return this.pubs[randomPubId];
    },
    updateNextPub: function () {
        this.pub = this.getAnotherRandomPub();
        this.pub_marker.setPosition(this.pub.location);
        this.pub_infowindow.setContent(this.getPubInfoWindowContent());

        for (var id in this.friends) {
            if (this.friends.hasOwnProperty(id)) {
                var friend = this.friends[id];
                var infowindow = this.friend_infowindows[friend.id];
                infowindow.setContent(this.getFriendInfoWindowContent(friend));
                infowindow.close();
            }
        }

        this.elements.pan_to_compromise_pub.html(this.pub.title + '<span class="pl-2">&rarr;</span>');

        this.clickOnMarker(this.pub_marker);
    },
    clickOnMarker: function(marker) {
        new google.maps.event.trigger(marker, 'click');
        this.map.panTo(marker.position);
    },
    googleMapsCallback: function() {
        var s = window.compromiseSettings;
        this.pubs = s.pubs;
        this.pub = s.pubs[s.pubId];
        this.friends = s.friends;
        this.area = s.area;
        this.shown_pubs = [s.pubId];

        this.elements.map = $('#map')
        this.elements.pan_to_compromise_pub = $('#pan_to_compromise_pub');
        this.elements.get_next_compromise_pub = $('#get_next_compromise_pub');
        this.elements.friends_links = $("a[id^='friend_']");

        this.init();
        this.showPubMarker();
        this.showFriends();
        this.setZoom();
        this.showArea();
        this.installPunToPubMarkerHandler();
        this.installGetNextPubHandler();
        this.installFriendsLinksHandler();
    }
};

module.exports = compromiseMap;