const $ = require('jquery');

var compromiseLocator = {
    elements: {
        form: '#locator-form',
        minRating: '#min-rating',
        minUserRatingsTotal: '#min-user-ratings-total',
        radius: '#radius',
        map: '#map',
        list: '#results-list'
    },
    input: {},
    currentUserPosition: null,
    map: null,
    locationMarker: null,
    areaCircle: null,
    request: null,
    pub_markers: {},
    pub_infowindows: {},
    init: function() {
        this.input = window.compromiseLocatorInput;

        this.initElements();
        this.updateResults();
        this.initEvents();
        this.initMap();

        if (window.compromiseLocatorDefaultLocation) {
            this.getUserLocation();
        }
    },
    getUserLocation: function() {
        var that = this;
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                that.currentUserPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }
                that.updateResults();
            });
        }
    },
    initElements: function() {
        for (var identifier in this.elements) {
            if (this.elements.hasOwnProperty(identifier)) {
                this.elements[identifier] = $(this.elements[identifier]);
            }
        }
    },
    initEvents: function() {
        var that = this;
        this.elements.minRating.on('change', function() {
            that.validateNumberInput(this);
            that.updateResults();
        });
        this.elements.minUserRatingsTotal.on('change', function() {
            that.validateNumberInput(this);
            that.updateResults();
        });
        this.elements.radius.on('change', function() {
            that.validateNumberInput(this);
            that.updateResults();
        });

        this.elements.list.on('click', 'a[data-place-id]', function() {
            that.listItemClicked(this);
        });
    },
    listItemClicked: function (el) {
        var el = $(el);
        var id = el.data('place-id');
        var marker = this.pub_markers[id];

        this.closeAllPubInfowindows();

        new google.maps.event.trigger(marker, 'click');
        this.map.panTo(marker.position);
    },
    validateNumberInput: function (el) {
        var el = $(el);
        var val = parseFloat(el.val());
        var min = parseFloat(el.attr('min'));
        var max = parseFloat(el.attr('max'));

        if (val < min) {
            el.val(min);
            return false;
        }

        if (val > max) {
            el.val(max);
            return false;
        }

        return true;
    },
    initMap: function() {
        this.map = new google.maps.Map(this.elements.map[0], {
            center: this.getLocation(),
            zoom: 15,
            disableDefaultUI: false,
            fullscreenControl: true
        });

        this.locationMarker = new google.maps.Marker({
            position: this.getLocation(),
            map: this.map,
            draggable: true,
            title: 'Your location',
            zIndex: 999
        });
        this.showAreaCircle();
        google.maps.event.addListener(this.locationMarker, 'dragend', this.updateResults.bind(this));
    },
    showAreaCircle: function() {
        if (this.areaCircle) {
            this.areaCircle.setMap(null);
        }

        this.areaCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.25,
            strokeWeight: 1,
            fillColor: '#FF0000',
            fillOpacity: 0.05,
            map: this.map,
            center: this.getLocation(),
            radius: this.input.radius
        });
        //this.map.fitBounds(this.areaCircle.getBounds());
    },
    updateResults: function() {
        if (this.request) {
            this.request.abort();
        }

        this.clearPubs();
        this.updateInput();
        this.showAreaCircle();

        var that = this;
        this.request = $.ajax({
            url: this.elements.form.data('get-results') + '?' + $.param(this.input),
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $.each(data.places, function(i, pub) {
                    that.addPubMarker(pub);
                    that.addPubListItem(pub);
                });
            },
            error: function(error){}
        });
    },
    updateInput: function() {
        this.input.min_rating = this.elements.minRating.val();
        this.input.min_user_ratings_total = this.elements.minUserRatingsTotal.val();
        this.input.radius = parseInt(this.elements.radius.val());

        if (this.locationMarker) {
            if (this.currentUserPosition) {
                this.locationMarker.setPosition(this.currentUserPosition);
                this.currentUserPosition = null;
            }

            var location = this.locationMarker.getPosition();
            this.input.lat = location.lat().toFixed(6);
            this.input.lon = location.lng().toFixed(6);

            this.map.panTo(location);
        }

        this.updateUrl();
    },
    clearPubs: function() {
        for (var id in this.pub_markers) {
            if (this.pub_markers.hasOwnProperty(id)) {
                this.pub_markers[id].setMap(null);
                this.pub_infowindows[id].setMap(null);
            }
        }

        this.pub_markers = [];
        this.pub_infowindows = [];

        this.elements.list.empty();
    },
    addPubMarker: function(p) {
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(p.lat), lng: parseFloat(p.lon)},
            map: this.map,
            title: p.name,
            icon: new google.maps.MarkerImage(
                '/icons/map.png',
                new google.maps.Size(25, 25),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 26),
                new google.maps.Size(25, 25)
            )
        });
        var infowindow = new google.maps.InfoWindow({
            content: this.getPubInfoWindowContent(p)
        });

        var that = this;
        marker.addListener('click', function() {
            that.closeAllPubInfowindows();
            infowindow.open(marker.get('map'), marker);
        });

        this.pub_markers[p.id] = marker;
        this.pub_infowindows[p.id] = infowindow;
    },
    addPubListItem: function(p) {
        var html = '<li>';
        html += '<a href="#' + this.elements.map.attr('id') + '" rel="nofollow" data-place-id="' + p.id + '">' + p.name + '<a/> ';
        html += '<span>Raiting: ' + p.rating + ', based on ' + p.user_ratings_total + ' reviews</span>';
        html += '<br /><span>' + p.address + '</span>';
        html += '</li>';
        this.elements.list.append(html);
    },
    closeAllPubInfowindows: function() {
        for (var id in this.pub_infowindows) {
            if (this.pub_infowindows.hasOwnProperty(id)) {
                this.pub_infowindows[id].close();
            }
        }
    },
    getPubInfoWindowContent: function(p) {
        var content = '<h5><a href="' + p.url + '" target="_blank" data-gtm="pub-info-title">' + p.name + '</a></h5>';
        if (p.address) {
            content += '<a href="' + p.directions_url + '" target="_blank" data-gtm="directions-pub">' + p.address + '</a><br />';
        }
        if (p.rating) {
            var priceLevel = '';
            if (p.price_level) {
                priceLevel = ', <span>' + p.price_level + '</span>';
            }
            content += 'Raiting: ' + p.rating + ', based on <a href="' + p.reviews_url + '" target="_blank" data-gtm="pub-info-reviews">' + p.user_ratings_total + '</a> reviews' + priceLevel;
        }
        if (p.website) {
            content += '<br /><a href="' + p.website + '" target="_blank" data-gtm="pub-info-website">' + p.website_name + '</a>';
        }
        return content;
    },
    updateUrl: function() {
        var title = document.getElementsByTagName('title')[0].innerHTML;
        var url = this.elements.form.attr('action');
        window.history.pushState( {} , title, url + '?' + $.param(this.input) );
    },
    googleMapsCallback: function() {
        this.init();
    },
    getLocation: function() {
        return {
            lat: parseFloat(this.input.lat),
            lng: parseFloat(this.input.lon)
        };
    }
};

module.exports = compromiseLocator;