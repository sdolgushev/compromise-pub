const $ = require('jquery');
require('bootstrap');


window.compromiseMap = require('./map');
window.compromiseLocator = require('./locator');

var friendsForm = require('./friends-form');
$(function() {
    friendsForm.init();
});

require('../css/app.scss');
require('../css/autocomplete.css');
