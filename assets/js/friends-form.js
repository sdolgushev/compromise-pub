const $ = require('jquery');
const autocomplete = require('autocomplete.js');
const slider = require("bootstrap-slider");

var friendsForm = {
    elements: {
        count: '#friends-count',
        containers: '.friend-container',
        add: '#add-friend',
        names: 'input.friends-name',
        postcodes: 'input.friends-postcode',
        removeLinks: 'a.remove-friend',
        minRating: '#min-rating',
        priceRange: '#price-range',
        minRatingLabel: '#min-rating-label'
    },
    selectors: {
        container: '.friend-container',
        nameInput: 'input.friends-name',
        postcodeInput: 'input.friends-postcode',
    },
    minFriends: 2,
    maxFriends: 8,
    possibleFriends: [
        "Alfred the Great",
        "Julie Andrews",
        "King Arthur",
        "David Attenborough",
        "Jane Austen",
        "Charles Babbage",
        "Lord Baden Powell",
        "Douglas Bader",
        "Neville Barnes Wallis",
        "David Beckham",
        "Alexander Graham Bell",
        "Tony Benn",
        "Tim Berners Lee",
        "Aneurin Bevan",
        "Tony Blair",
        "William Blake",
        "William Booth",
        "Boudicca",
        "David Bowie",
        "Richard Branson",
        "Robert the Bruce",
        "Isambard Kingdom Brunel",
        "Richard Burton",
        "Donald Campbell",
        "William Caxton",
        "Charlie Chaplin",
        "Geoffrey Chaucer",
        "Leonard Cheshire",
        "Winston Churchill",
        "James Connolly",
        "Captain James Cook",
        "Michael Crawford",
        "Oliver Cromwell",
        "Aleister Crowley",
        "Charles Darwin",
        "Diana, Princess of Wales",
        "Charles Dickens",
        "Francis Drake",
        "King Edward I",
        "Edward Elgar",
        "Queen Elizabeth I",
        "Queen Elizabeth II",
        "Queen Elizabeth, the Queen Mother",
        "Michael Faraday",
        "Guy Fawkes",
        "Alexander Fleming",
        "Bob Geldof",
        "Owain Glyndwr",
        "George Harrison",
        "John Harrison",
        "Stephen Hawking",
        "King Henry II",
        "King Henry V",
        "King Henry VIII",
        "Paul Hewson (Bono)",
        "Edward Jenner",
        "TE Lawrence",
        "John Lennon",
        "David Livingstone",
        "David Lloyd George",
        "John Logie Baird",
        "John Lydon (Johnny Rotten)",
        "James Clerk Maxwell",
        "Paul McCartney",
        "Freddie Mercury",
        "Field Marshal Bernard Montgomery",
        "Bobby Moore",
        "Thomas More",
        "Eric Morecambe",
        "Admiral Horatio Nelson",
        "Isaac Newton",
        "Florence Nightingale",
        "George O'Dowd (Boy George)",
        "Thomas Paine",
        "Emmeline Pankhurst",
        "John Peel",
        "Enoch Powell",
        "Walter Raleigh",
        "Steve Redgrave",
        "King Richard III",
        "Cliff Richard",
        "JK Rowling",
        "Robert Falcon Scott",
        "Ernest Shackleton",
        "William Shakespeare",
        "George Stephenson",
        "Marie Stopes",
        "Margaret Thatcher",
        "William Tindale",
        "JRR Tolkien",
        "Alan Turing",
        "Unknown soldier",
        "Queen Victoria",
        "William Wallace",
        "James Watt",
        "Duke of Wellington",
        "John Wesley",
        "Frank Whittle",
        "William Wilberforce",
        "Robbie Williams",
        "Roger Bannister",
        "Emily Bronte",
        "Robert Burns",
        "Prince Charles",
        "John Constable",
        "John Keats",
        "Mary, Queen of Scots",
        "Laurence Olivier",
        "Lord Reith",
        "JMW Turner"
    ],
    possiblePostcodes: [
        "SE17 1EX",
        "SE11 4EA",
        "SE11 4RN",
        "SE11 6SF",
        "SE1 6ER",
        "SE1 7QX",
        "SW1A 0AA",
        "EC3M 8AF",
        "SE1 9TG",
        "WC2H 7NA"
    ],
    init: function() {
        this.initElements();
        if (this.elements.count.length > 0) {
            this.updatePlaceholders();
            this.showFriends();
            this.initSliders();
            this.initEvents();
            this.installAutocomplete();
        }
    },
    initElements: function() {
        for (var identifier in this.elements) {
            if (this.elements.hasOwnProperty(identifier)) {
                this.elements[identifier] = $(this.elements[identifier]);
            }
        }
    },
    updatePlaceholders: function() {
        this.setPlaceholders(this.elements.names, this.possibleFriends);
        this.setPlaceholders(this.elements.postcodes, this.possiblePostcodes);
    },
    setPlaceholders: function(elements, possibleValues) {
        elements.each(function() {
            var el = $(this);
            if (el.attr('placeholder')) {
                return true;
            }

            if (possibleValues.length === 0) {
                return flase;
            }

            var index = Math.floor(Math.random()*possibleValues.length);
            var friend = possibleValues[index];
            el.attr('placeholder', friend);
            possibleValues.splice(index, 1);
        });
    },
    showFriends: function() {
        var number = parseInt(this.elements.count.val());
        this.elements.containers.each(function() {
            var el = $(this);
            if (el.attr('data-id') > number) {
                $('input', el).attr('disabled', 'disabled');
                el.hide();
            } else {
                $('input', el).removeAttr('disabled');
                el.show();
            }
        });

        this.updateRemoveFriendLinks();
    },
    addFriend: function() {
        $(this.selectors.container + ':hidden:first').show();

        var count = parseInt(this.elements.count.val()) + 1;
        this.elements.count.val(count);

        if (count >= this.maxFriends) {
            this.elements.add.hide();
        }

        this.showFriends();
    },
    removeFriend: function(link) {
        var container = link.closest(this.selectors.container);
        container.hide();

        $(this.selectors.nameInput, container).val('');
        $(this.selectors.postcodeInput, container).val('');

        $(this.selectors.container + ':last').after(container);

        this.elements.count.val(parseInt(this.elements.count.val()) - 1);
        this.elements.add.show();

        this.showFriends();
    },
    updateRemoveFriendLinks: function() {
        this.elements.removeLinks.hide();
        if (parseInt(this.elements.count.val()) > this.minFriends) {
            this.elements.removeLinks.show();
        }
    },
    initSliders: function() {
        this.elements.minRating.slider({});
        this.elements.priceRange.slider({});
    },
    initEvents: function() {
        var that = this;

        this.elements.count.on('change', this.showFriends.bind(this));
        this.elements.minRating.on('change', function() {
            that.elements.minRatingLabel.html($(this).val());
        });
        this.elements.minRating.change();
        this.elements.removeLinks.on('click', function(e) {
           e.preventDefault();
           that.removeFriend($(this));
        });

        this.elements.add.on('click', function(e) {
           e.preventDefault();
            that.addFriend();
        });
    },
    installAutocomplete: function() {
        var that = this;
        autocomplete('input.friends-postcode', { hint: false }, [
            {
                source: that.getAutocompleteSuggestions.bind(this),
                displayKey: 'postcode',
                templates: {
                    suggestion: function(suggestion) {
                        return suggestion.postcode;
                    }
                }
            }
        ]);
        this.elements.postcodes.attr('autocomplete', 'new-password');
    },
    getAutocompleteSuggestions: function(query, callback) {
        var url = $(this.elements.postcodes.get(0)).data('autocomplete-url').replace('/query', '/' + query);
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                callback(data.suggestions);
            },
            error: function(error){
                callback([]);
            }
        });
    }
}

module.exports = friendsForm;