# Compromise Pub

My first Symfony 4 project, which helps to solve one of the most important problems in any Londoner's life.

## Installation

1. Clone the sources:
    ```bash
    $ cd <PROJECTS_PATH>
    $ git clone git@gitlab.com:sdolgushev/compromise-pub.git
    ```
2. Install dependencies:
    ```bash
    $ composer install
    ```
3. Create a new database (I know it is not the best practice to keep dump in the repo, but it makes life much simpler):
    ```bash
    mysql -u root -e "CREATE DATABASE compromisepub CHARACTER SET utf8 COLLATE utf8_general_ci"
    cat dump.sql | mysql -u root compromisepub
    ```
4. Update parameters in `.env.local`:
    ```bash
    GOOGLE_PLACES_API_KEY=<API_KEY>
    GOOGLE_MAPS_API_KEY=<API_KEY>
    DATABASE_URL=<mysql://root:@localhost:3306/compromisepub>
    ```
5. Start local server:
    ```bash
    $ bin/console server:start

    [OK] Server listening on http://127.0.0.1:8001
    ``` 
6. Open [http://127.0.0.1:8001](http://127.0.0.1:8001) in your browser. Done.

## Updating index

It is done via cronjobs. And they are using [Google Places API](https://developers.google.com/places/web-service/intro). Please [check prices](https://developers.google.com/places/web-service/usage-and-billing) and setup spending limits to keep your wallet in a good shape. There are two main cronjobs:

1. Exploring new pubs and getting their base details. In case if all places are explored, try to increase `App\Command\ExplorePlacesCommand::EXPLORE_COVERAGE_RADIUS`: 
    ```bash
    bin/console app:explore-places --api_requests_limit=9
    ```
2. Get details for explored pubs:
    ```bash
    bin/console app:update-places --limit=30
    ```
    
You might be interested in [server-config/crontab](https://gitlab.com/sdolgushev/compromise-pub/blob/master/server-configs/crontab).